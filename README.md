Welcome to front libs of data cafe !
Here i hope you find the libs you want to use on your project !!

# pnpm installation
In the root directory, run :
```
pnpm install
```
It will check the inscruction in each workspace projects and create the node_modules directory. 

# jest setup
To allow jest to run your unit tests, follow this link (don't forget to replace `npm` with `pnpm`):
https://timdeschryver.dev/blog/integrate-jest-into-an-angular-application-and-library#adding-jest
