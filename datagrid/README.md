# Datagrid

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

## Development

First, build the datagrid lib in a terminal:

```sh
pnpm watch
```

Second, see result while coding in **another** terminal:

```sh
pnpm playgroud
```

Third, you can start coding.

## Build & publish

First, increment the version number into `datagrid/projects/datagrid/package.json`.

Second, build the datagrid lib for production:

```sh
pnpm build
```

It will generate artifacts in the `dist/datagrid` directory.

Third, publish new version:

```sh
pnpm publish
```

## Running unit tests

Run `pnpm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
