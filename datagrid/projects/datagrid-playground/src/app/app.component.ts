/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {Component} from '@angular/core';
import {Item, items, texts} from "./app.constant";
import {EditOutput} from "datagrid";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly texts = texts;
  readonly items = items;

  readonly controls = {
    editable: false,
  }

  onEdit($event: EditOutput<Item>): void {
    console.log($event);
    (items as Array<Item>)
      .filter(item => item.id === $event.previous.id)
      .forEach(item => {
        $event.updatedProperties.forEach(property => {
          // @ts-ignore
          item[property] = $event.updated[property];
        });
      });
  }
}
