/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { TextsInput} from "datagrid";

export const texts: TextsInput = {
  h2: 'Datagrid component',
  top: 'This is a simple test page for the datagrid component. You can use it to test the component and the different features.',
  bottom: 'Made with 💖 by Data Terrae',
  counter: (c) => c ? c > 1 ? `${c} items` : '1 item' : 'No items',
};

export interface Item {
  id: number;
  name: string;
  description: string;
}

export const items: Array<Item> = [
  {id: 1, name: 'Item 1', description: 'This is the first item'},
  {id: 2, name: 'Item 2', description: 'This is the second item'},
  {id: 3, name: 'Item 3', description: 'This is the third item'},
  {id: 4, name: 'Item 4', description: 'This is the fourth item'},
  {id: 5, name: 'Item 5', description: 'This is the fifth item'},
  {id: 6, name: 'Item 6', description: 'This is the sixth item'},
  {id: 7, name: 'Item 7', description: 'This is the seventh item'},
  {id: 8, name: 'Item 8', description: 'This is the eighth item'},
];
