/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';
import { FormControl, ValidatorFn } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import { ICONS } from '../constant';

export interface Result<T> {
  previous: T | null,
  current: T | null,
}

@UntilDestroy()
@Component({
  selector: 'dt-cell-editable',
  templateUrl: './cell-editable.component.html',
  styleUrls: ['./cell-editable.component.scss']
})
export class CellEditableComponent<T> implements OnInit {

  readonly ICON = ICONS;

  /**
   * Main item to edit.
   * It could be null, meaning no value.
   * The `undefined` value is not allowed here; please use `null` instead.
   */
  @Input() item: T | null = null;
  /**
   * Fallback label used when item is null or falsy, or the property is missing.
   *
   * This is optional.
   */
  @Input() fallbackLabel?: string;
  /**
   * This list is a range of allowed values.
   * It displays a select element instead of a simple input.
   *
   * This is optional.
   */
  @Input() altValues?: Array<T>;
  /**
   * Converter method.
   *
   * This is optional only if:
   * - the item is a string
   * - the item contains a property `label`, `name` or `title`
   */
  @Input() itemToLabel?: (t: T) => string;

  /**
   * Allow edition
   */
  @Input() enable = true;

  /**
   * Specify some extra validators
   */
  @Input() validators: Array<ValidatorFn> = [];

  /**
   * call on save:
   * - button validate
   * - enter key
   */
  @Output() save: EventEmitter<Result<T>> = new EventEmitter<Result<T>>();

  /**
   * Dispatch a event indicating whenever this component is in edition mode.
   */
  @Output() editing$: EventEmitter<boolean> = new EventEmitter<boolean>();

  formControl = new FormControl();

  constructor(readonly renderer: Renderer2,
              readonly element: ElementRef) {
  }

  ngOnInit(): void {
    // Styling parent cell <td>
    const cellElement = this.element.nativeElement.closest('clr-dg-cell');
    this.renderer.addClass(cellElement, 'has-edit');
  }

  getLabel(value: T | null): string | undefined {
    if (!value) {
      return undefined;
    } else if (['string', 'number'].includes(typeof value)) {
      return `${value}`;
    } else if (this.itemToLabel) {
      return this.itemToLabel(value);
    } else if ('label' in value) {
      return (value as unknown as { label: string }).label;
    } else if ('name' in value) {
      return (value as unknown as { name: string }).name;
    } else if ('title' in value) {
      return (value as unknown as { title: string }).title;
    } else {
      return '' + value;
    }
  }

  onEditableChange(mode: 'view' | 'edit'): void {
    if (mode === 'edit') {
      this.formControl.setValidators(this.validators);
      this.formControl.setValue(this.item);
    }
    this.editing$.emit(mode === 'edit');
  }

  trim(value: T | null): T | null {
    if (typeof value === 'string') {
      return value.trim() as unknown as T;
    } else {
      return value;
    }
  }

  onEdit() {
    if (this.formControl.valid) {
      this.save.emit({
        previous: this.item ?? null,
        current: this.trim(this.formControl.value) ?? null,
      });
    }
  }
}
