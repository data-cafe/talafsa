/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { Validators } from '@angular/forms';

export const EDITABLE_VALIDATORS = {
  NONE: [],
  REQUIRED: [Validators.required],
  EMAIL: [Validators.required, Validators.email],
};
