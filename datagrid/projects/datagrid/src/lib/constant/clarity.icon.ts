/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { checkIcon, timesIcon } from '@cds/core/icon';
import { prepareIcons } from '@data-cafe/helpers';

export const { IconsForTemplate: ICONS, IconsForImport: IMPORT_ICONS } =
  prepareIcons({
    VALID: checkIcon,
    CANCEL: timesIcon,
  });
