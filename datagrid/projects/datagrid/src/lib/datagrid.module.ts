/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityIcons } from '@cds/core/icon';
import { ClarityModule } from '@clr/angular';
import { EditableModule } from '@ngneat/edit-in-place';
import { DatagridComponent } from './datagrid';
import { CellEditableComponent } from './cell-editable';
import { IMPORT_ICONS } from './constant';
import { uniqueness } from '@data-cafe/helpers';

// Clarity web component
ClarityIcons.addIcons(...uniqueness(...IMPORT_ICONS));

@NgModule({
  declarations: [DatagridComponent, CellEditableComponent],
  imports: [
    // Angular
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    // Third-party
    ClarityModule,
    EditableModule,
  ],
  exports: [DatagridComponent],
})
export class DatagridModule {}
