/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClrLoadingState } from '@clr/angular';
import { UntilDestroy } from '@ngneat/until-destroy';
import { ActionsInput, ItemsInput, TextsInput } from '../io';
import { EditOutput } from '../io/edit.output';
import { Column, Data, initialUI, Item, Type, UI } from '../model';

@UntilDestroy()
@Component({
  selector: 'dt-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss'],
})
export class DatagridComponent<I extends Item> {
  readonly NO_ITEMS: Array<I> = [];
  readonly NO_COLS: Array<Column> = [];
  readonly ClrLoadingState = ClrLoadingState;

  data?: Data<I>;
  ui: UI = initialUI();

  // -- Lifecycle --------------------------------------------------------------

  /**
   * Customize all texts in component for i18n purposes.
   */
  @Input() texts?: TextsInput;

  /**
   * Define custom action buttons.
   */
  @Input() actions?: ActionsInput;

  /**
   * Dispatch everytime an item is updated in the datagrid.
   *
   * @see editable
   */
  @Output() edit = new EventEmitter<EditOutput<I>>();

  constructor() {
  }

  /**
   * Set the data to be displayed.
   * @param value
   */
  @Input() set items(value: ItemsInput<I> | undefined | null) {
    this.updateUI(value ?? undefined);
  }

  /**
   * Transform the grid editable.
   *
   * @param something
   *
   * @see edit
   */
  @Input() set editable(something: unknown) {
    this.ui.editable = something != null && `${something}` !== "false";
  }

  onEdit(item: I, property: string, modification: any): void {
    this.edit.emit({
      previous: { ...item },
      updated: {
        ...item,
        [property]: modification,
      },
      updatedProperties: [property],
    });
  }

  // -- Helper methods ---------------------------------------------------------

  private updateUI(items?: Array<I>) {
    if (items?.length) {
      // Generate structure
      const cols: Column[] = Object
        .keys(items[0])
        .map(property => (<Column>{
          property,
          title: property, // TODO: Capitalize first letter
          type: Type.TEXT,
        }));

      // Build data
      this.data = {
        items,
        cols,
      };

      // Update UI
      this.ui.loading = false;

    } else {
      // Clear data
      this.data = undefined;

      // Update UI
      this.ui.loading = true;
    }
  }
}
