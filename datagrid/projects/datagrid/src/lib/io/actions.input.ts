/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { Action } from '../model/actions.model';

/**
 * List of actions.
 */
export type ActionsInput = Array<Action>;
