/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

/**
 * Text wrapper to customise the page
 */
export interface TextsInput {
  /**
   * Title H1 of the page
   */
  h1?: string;

  /**
   * Title H2 of the page
   */
  h2?: string;

  /**
   * Title H4 of the page
   */
  h3?: string;

  /**
   * Top text of the page
   */
  top?: string;

  /**
   * Datagrid placeholder
   */
  placeholder?: string;

  /**
   * Fallback label in datagrid if a cell miss a value.
   */
  fallbackLabel?: string;

  /**
   * Datagrid item count in footer (pagination part).
   */
  counter?: (count?: number) => string;

  /**
   * Bottom text of the page
   */
  bottom?: string;
}
