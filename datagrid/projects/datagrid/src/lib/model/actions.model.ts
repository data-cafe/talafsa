/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { ClrLoadingState } from '@clr/angular';

/**
 * Action description.
 */
export interface Action {
  /**
   * The button text.
   */
  label: string;

  /**
   * The button icon shape of Clarity Icon.
   */
  icon?: string;

  /**
   * The button state.
   * @see ClrButton
   * @see ClrLoadingState
   */
  loading?: ClrLoadingState;

  /**
   * Callback function to execute when the action is clicked.
   *
   * @param $event The click event (mouse or keyboard)
   */
  callback: ($event: UIEvent) => void;
}
