/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { Type } from './type.enum';

/**
 * Column definition
 */
export interface Column {
  /**
   * Property name, aka reference in the item
   */
  property: string;

  /**
   * Column header label
   */
  title: string;

  /**
   * Label used for cells of the column when a value is missing (`null` or `undefined`).
   */
  fallbackLabel?: string;

  /**
   * Value type, aka type of the property in the item
   */
  type: Type;
}
