/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { Column } from './columns.model';
import { Item } from './items.model';

export interface Data<I extends Item> {
  items: Array<I>;
  cols: Array<Column>;
}
