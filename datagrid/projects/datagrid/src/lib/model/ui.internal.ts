/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

/**
 * Internal variable to command UI rendering.
 */
export interface UI {
  loading: boolean;
  editable: boolean;
}

/**
 * Generate the inital UI state.
 */
export function initialUI(): UI {
  return {
    loading: false,
    editable: false,
  };
}
