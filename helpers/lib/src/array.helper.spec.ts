/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import {sortDatePropertyAscendantly, sortNumberAscendantly, sortStringPropertyAscendantly, uniqueness} from './array.helper';

let numArray = [8, 4, 12, 28, 6614, 186546652, 2, 64.5];
let stringArray = ["Hello", "World", "!", "Hello", "World"];
let badArray = [undefined, null, "", null, undefined, ""]
let stringsWithproperties: Array<Record<string, string>> = [{hello: "Hello", world: "World", exclamationMark: "!"}, {hello: "Salut", world: "Monde", exclamationMark: "?"}, {hello: "Buongiorno", world: "Mondo", exclamationMark: ";"}];
let stringsWithDates: Array<Record<string, Date>> = [{date: new Date(2022 - 21 - 3)}, {date: new Date(2022 - 21 - 1)}, {date: new Date(2022 - 21 - 2)}];
let stringsWithBadDates: Array<Record<string, Date>> = [{date: undefined as any as Date}, {date: null as any as Date}, {date: "" as any as Date}, {date: 0 as any as Date}];
let stringsWithBadproperties: Array<Record<string, string>> = [{hello: undefined}, {hello: null}, {hello: ""}]

describe('array helper functions tests', () => {
  describe('uniqueness funtion test', () => {
    it('Should return array with unique values', () => {
      stringArray = uniqueness<string>(...stringArray);
      expect(stringArray.length).toBe(3);
      expect(stringArray[0]).toBe("Hello");
      expect(stringArray[1]).toBe("World");
      expect(stringArray[2]).toBe("!");
      expect(stringArray[3]).toBeUndefined();
    });

    it('should work with unusual values', () => {
      badArray = uniqueness<string>(...badArray);
      expect(badArray.length).toBe(3);
      expect(badArray).toEqual([undefined, null, ""]);
    });

    it('should work with unusual values', () => {
      let badArray2 = uniqueness<string>(...[]);
      expect(badArray2.length).toBe(0);
      expect(badArray2).toEqual([]);
    });
  });


  describe('Sort number arrays', () => {
    it('Should return a ascendently sorted array ', () => {
      let result = numArray.sort(sortNumberAscendantly);
      let previousNum = result[0];
      result.forEach(num => {
        if (previousNum) {
          expect(num).toBeGreaterThanOrEqual(previousNum);
        }
        previousNum = num;
      });
    });
  });

  describe('Should sort date property ascendently', () => {
    it('should sort objects by a specific value', () => {
      expect(stringsWithDates.sort(sortDatePropertyAscendantly("date"))).toEqual([{date: new Date(2022 - 21 - 1)}, {date: new Date(2022 - 21 - 2)}, {date: new Date(2022 - 21 - 3)}]);
    });

    it('should return the same array with bad properties', () => {
      expect(stringsWithBadDates.sort(sortDatePropertyAscendantly("date"))).toEqual(stringsWithBadDates);
    });
  });

  describe('Sort string property ascendently', () => {
    it('should sort objects by a specific value', () => {
      expect(stringsWithproperties.sort(sortStringPropertyAscendantly("hello"))).toEqual([{hello: "Buongiorno", world: "Mondo", exclamationMark: ";"}, {hello: "Hello", world: "World", exclamationMark: "!"}, {hello: "Salut", world: "Monde", exclamationMark: "?"}]);
    });

    it('should return the same array with bad properties', () => {
      expect(stringsWithBadproperties.sort(sortStringPropertyAscendantly("hello"))).toEqual(stringsWithBadproperties);
    });

  });
});
