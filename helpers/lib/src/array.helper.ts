/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import naturalCompare from "string-natural-compare";
import {Pot} from "./type.helper";

/**
	* Compare function for array sort.
	*/
export type Sort<T> = (a: T, b: T) => number;

/**
	* Ensure uniqueness of values in a array.
	*
	* Note: this method uses the ES6 Set method, the quickest according
	* https://stackoverflow.com/a/43046408
	*
	* @param values a list of values
	* @return Array<T> an array with unique values
	* @see https://stackoverflow.com/a/43046408
	* @see Set
	*/
export function uniqueness<T>(...values: Array<T>): Array<T> {
	return [...new Set(values)];
}

/**
	* Sort an array of number ascendantly.
	* @param a the first number
	* @param b the second number
	* @return number the sort order
	*/
export const sortNumberAscendantly: Sort<number> =
	(a: number, b: number) => a - b;


/**
	* Sort an array of string ascendantly.
	* @param a the first string
	* @param b the second string
	* @return number the sort order
	*/
export const sortStringAscendantly: Sort<Pot<string>> =
	(a: Pot<string>, b: Pot<string>) => naturalCompare(a ?? '', b ?? '');

/**
	* Sort an array of string ascendantly ignoring case.
	* @param a the first string
	* @param b the second string
	* @return number the sort order ignoring case
	*/
export const sortStringAscendantlyCaseInsensitive: Sort<Pot<string>> =
	(a: Pot<string>, b: Pot<string>) => naturalCompare(a ?? '', b ?? '', {caseInsensitive: true});

/**
	* Sort an array of object ascendantly comparing a string property.
	*
	* @param property a property typed as string and optional (undefined or null)
	* @param caseInsensitive set to true to compare strings case-insensitively. Default: false
	* @return CompareFunction the comparison method that compare an array of object
	*/
export function sortStringPropertyAscendantly<P extends string, T extends Record<P, string>>(
	property: P, caseInsensitive: boolean = false): Sort<T> {
	return (a: T, b: T) => naturalCompare(a[property] ?? '', b[property] ?? '', {caseInsensitive});
}

/**
	* Sort an array of object ascendantly comparing a date property.
	*
	* @param property a property typed as date and optional (undefined or null)
	* @return CompareFunction the comparison method that compare an array of object
	*/
export function sortDatePropertyAscendantly<P extends string, T extends Record<P, Date>>(
	property: P): Sort<T> {
	return (a: T, b: T) => +(b[property] ?? 0) - +(a[property] ?? 0);
}
