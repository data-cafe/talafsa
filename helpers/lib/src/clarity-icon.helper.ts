/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {IconShapeTuple} from "@cds/core/icon/interfaces/icon.interfaces";

type BaseIcons = Record<string, IconShapeTuple>;

type TemplateIcons<T extends BaseIcons> = Record<keyof T, string>;

interface ArrangeResult<T extends BaseIcons> {
	/**
		* The main goal is centralisation of icon
		*/
	IconsForTemplate: TemplateIcons<T>;

	/**
		* Helper for importing Clarity icons
		*/
	IconsForImport: Array<IconShapeTuple>;
}

/**
	* Prepare Clarity icons into two sets:
	* - a set ready to used within templating
	* - a set ready to used for import
	*
	* @param icons a list of icons referenced with an arbitrary key
	*/
export function prepareIcons<T extends BaseIcons>(icons: T): ArrangeResult<T> {
	return {
		IconsForTemplate: Object
			.entries(icons)
			.reduce((acc, [key, value]) => ({...acc, [key]: value[0]}), {} as TemplateIcons<T>),
		IconsForImport: Object
			.keys(icons)
			.map(key => icons[key]),
	}
}

