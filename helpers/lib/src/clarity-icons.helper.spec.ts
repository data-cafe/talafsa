/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {IconShapeTuple} from "@cds/core/icon/interfaces/icon.interfaces";
import {prepareIcons} from "./clarity-icon.helper";

const bellIcon: IconShapeTuple = ["bell", "bellSVG"];
const cogIcon: IconShapeTuple = ["cog", "cogSVG"];
const usersIcon: IconShapeTuple = ["users", "usersSVG"];

const icons: Record<string, IconShapeTuple> = {
  BELL: bellIcon,
  COG: cogIcon,
  USERS: usersIcon,
};

const badbellIcon = undefined as any as IconShapeTuple;
const badcogIcon = null as any as IconShapeTuple;

const badIconsArray = [{BELL: badbellIcon}, {COG: badcogIcon}];

describe('prepareIcons function test', () => {

  it('Should return a valid object for clarity import', () => {
    expect(prepareIcons(icons)).toEqual({
      IconsForTemplate: {BELL: 'bell', COG: 'cog', USERS: 'users'},
      IconsForImport: [
        ['bell', 'bellSVG'],
        ['cog', 'cogSVG'],
        ['users', 'usersSVG']
      ]
    });
  });

  it('Should return a valid object for clarity-icons import', () => {
    badIconsArray.forEach(badIcon => {
      expect(() => {
        prepareIcons(badIcon);
      })
        .toThrow();
    });
  });


});
