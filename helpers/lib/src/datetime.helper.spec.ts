import {fixChunkDate, fixChunkTimeStamp, fixDate, fixTimeStamp} from "./datetime.helper";

//good values objects (GMT+1)
const javaTimeStampDateOBJ = {date: 1640991600 as any as Date, stringDate: "1rst January 2022"};
const javaTimeStampOBJ = {date: 1640991600, stringDate: "1rst January 2022"};
const javaChunkTimeStampOBJ = [{date: 1640991600, stringDate: "1rst January 2022"}, {date: 1641078000, stringDate2: "2nd January 2022"}];
const dateOBJ = {date: "2022-1-1 00:00:00 UTC" as any as Date, stringDate: "1rst January 2022"};
const chunkDateOBJ = [{date: "2022-1-1 00:00:00 UTC" as any as Date, stringDate: "1rst January 2022"}, {date: "2022-1-2 00:00:00 UTC" as any as Date, stringDate2: "2nd January 2022"}];
const JSTimeStampOBJ = {date: 1640991600000, stringDate: "1rst January 2022"};
const JSChunkTimeStampOBJ = [{date: 1640991600000, stringDate: "1rst January 2022"}, {date: 1641078000000, stringDate2: "2nd January 2022"}];

//Bad values objects
const zeroOBJ = {date: 0 as any as Date, stringDate: "1rst January 2022"};
const nullOBJ = {date: null, stringDate: "1rst January 2022"};
const undefinedOBJ = {date: undefined, stringDate: "1rst January 2022"};
const emptyOBJ = {date: "" as any as Date, stringDate: "1rst January 2022"};

describe('Datetime helpers tests', () => {
  describe('Fixdate function tests', () => {
    it('Should return unmodified object with bad value', () => {
      expect(fixDate(nullOBJ, "date")).toEqual(nullOBJ);
      expect(fixDate(undefinedOBJ, "date")).toEqual(undefinedOBJ);
      expect(fixDate(zeroOBJ, "date")).toEqual(zeroOBJ);
      expect(fixDate(emptyOBJ, "date")).toEqual(emptyOBJ);
    });

    it('Should work with timestamp in Java format (not in JS format)', () => {
      expect(fixDate(javaTimeStampDateOBJ, "date")["date"]).toEqual(new Date(1640991600000))
    });

    it('Should work with correct data object', () => {
      expect(fixDate(dateOBJ, "date")["date"]).toEqual(new Date("2022-01-01T00:00:00.000Z"))
    });
  });

  it('fixChunkDate', () => {
    let result = fixChunkDate(chunkDateOBJ, "date");
    expect(result[0]["date"]).toEqual(new Date("2022-01-01T00:00:00.000Z"));
    expect(result[1]["date"]).toEqual(new Date("2022-01-02T00:00:00.000Z"))
  });

  it('fixTimeStamp', () => {
    expect(fixTimeStamp(javaTimeStampOBJ, "date")).toEqual(JSTimeStampOBJ)
  });

  it('fixChunkTimeStamp', () => {
    expect(fixChunkTimeStamp(javaChunkTimeStampOBJ, "date")).toEqual(JSChunkTimeStampOBJ)
  });
});
