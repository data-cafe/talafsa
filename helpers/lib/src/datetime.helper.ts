/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

type HasDate<P extends string> = { [p in P]: Date };
type HasTimestamp<P extends string> = { [p in P]: number };

const THRESHOLD_JAVA_TIMESTAMP = 10000000000;
const DIFF_JAVA_TIMESTAMP = 1000;

export function fixDate<P extends string, T extends HasDate<P>>
(object: T, ...properties: Array<P>): T {
    properties.forEach(property => {
        if ([0, "", null, undefined].includes(object[property] as any)) {
            // Server gives invalid value
            // do nothing: let value as given
            return;
        } else if (typeof object[property] === "number" && (object[property] as unknown as number) < THRESHOLD_JAVA_TIMESTAMP) {
            // Server gives timestamp in Java format (not in JS format)
            object[property] = new Date((object[property] as unknown as number) * DIFF_JAVA_TIMESTAMP) as T[P];
        } else {
            // @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date#parameters
            // Server gives:
            //  - a correct Date object
            //  - a string formatted date
            //  - a timestamp in JS format
            object[property] = new Date(object[property]) as T[P];
        }
    });
    return object;
}

export function fixChunkDate<P extends string, T extends HasDate<P>, Ts extends Array<T>>
(objects: Ts, ...properties: Array<P>): Ts {
    return objects.map(object => fixDate(object, ...properties)) as Ts;
}


export function fixTimeStamp<P extends string, T extends HasTimestamp<P>>
(object: T, ...properties: Array<P>): T {
    properties.forEach(property => {
        object[property] = (object[property] < THRESHOLD_JAVA_TIMESTAMP
                ? object[property] * DIFF_JAVA_TIMESTAMP
                : object[property]
        ) as T[P];
    });
    return object;
}

export function fixChunkTimeStamp<P extends string, T extends HasTimestamp<P>, Ts extends Array<T>>
(objects: Ts, ...properties: Array<P>): Ts {
    return objects.map(object => fixTimeStamp(object, ...properties)) as Ts;
}

