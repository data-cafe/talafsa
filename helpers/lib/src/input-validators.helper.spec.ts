/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import {Component} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from '@angular/platform-browser-dynamic/testing';
import {ValidRegexValidator} from './input-validators.helper';

describe('Test custom validators', () => {

  describe('Test valid regex validator', () => {

    let testComponent: TestComponent;
    let testFixture: ComponentFixture<TestComponent>;

    //This component encapsulate settings-form component to test it in the right condition i.e with @Input & @Output
    @Component({
      template: `<input type="text" [formControl]="regex_filter">`
    })
    class TestComponent {
      regex_filter = new FormControl('', ValidRegexValidator());
    }

    beforeEach(async () => {
      TestBed.resetTestEnvironment();
      TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());

      await TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
        ],
        declarations: [
          TestComponent
        ],
      })
        .compileComponents();

      testFixture = TestBed.createComponent(TestComponent);
      testComponent = testFixture.componentInstance;
    });

    it('should create an instance', () => {
      expect(testComponent).toBeTruthy();
    });

    ["[", "\\", "/[-)", "[0-9]++"]
      .forEach((value) => {
        it(`input should not be valid with ${value}`, () => {
          testComponent.regex_filter.setValue(value);
          expect(testComponent.regex_filter.status).toEqual("INVALID");
          expect(testComponent.regex_filter.errors).toEqual({regexValidityError: true});
        });
      });

    ["^[0-9]$", /^[0-9]$/, "Nancy", /^\w+@\w+..{2,3}(.{2,3})?$/, /Nancy/, ".", /./, null, undefined, ""]
      .forEach((value) => {
        it(`input should be valid with ${value}`, () => {
          testComponent.regex_filter.setValue(value);
          expect(testComponent.regex_filter.status).toEqual("VALID");
          expect(testComponent.regex_filter.errors).toBeNull();

        });
      });
  });
});
