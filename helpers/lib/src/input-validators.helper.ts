/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

export interface RegexValidationError {
  regexValidityError: boolean;
}
export interface InputControl {
  value: string | RegExp | null | undefined;
}
export type RegexValidatorFn = (control: InputControl) => RegexValidationError | null;

// This validator only checks for a valid regex so null, undefined and and empty string will work
export function ValidRegexValidator(): RegexValidatorFn {
  return (control: InputControl): RegexValidationError | null => {

    const value = control.value;

    if (value) {
      try {
        new RegExp(value);
      } catch (error) {
        return {regexValidityError: true};
      }
    }

    return null;
  };
}
