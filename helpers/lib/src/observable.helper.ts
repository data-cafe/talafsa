/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {combineLatest, Observable} from 'rxjs';
import {untilDestroyed} from '@ngneat/until-destroy';
import {map} from 'rxjs/operators';

/**
	* Combine two observables
	*
	* @param untilDestroyRef UntilDestroy component reference
	* @param source1 first source of data
	* @param source2 second source of data
	* @param combinator way to combine data
  * @see combineLatest
	*/
export function combine<C, T, U, R>(untilDestroyRef: C,
  source1: Observable<T>,
  source2: Observable<U>,
  combinator: (c: [T, U]) => R): Observable<R> {
	return combineLatest([source1, source2]).pipe(
		untilDestroyed(untilDestroyRef),
		map(combinator),
	);
}
