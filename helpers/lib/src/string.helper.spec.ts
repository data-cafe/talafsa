/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {labelize} from './string.helper';

it('should work', () => {
  ["oh_hi_mark!", "LiB-hElPeRs_DaTa/CaFé", "oh-hi-mark!", "oh hi mark!", "18-12-2022"].forEach(testString => {

    expect(typeof labelize(testString)).toBe("string");
    expect(labelize(testString).split(" ").length).toBeGreaterThan(2);
    expect(labelize(testString).includes("-")).toBeFalsy();
    expect(labelize(testString).includes("_")).toBeFalsy();
    expect(labelize(testString).includes(" ")).toBeTruthy();

    labelize(testString).split(" ").forEach(word => {
      expect(word[0]).toEqual(word[0].toUpperCase());
    });

  });
});

it('should throw error', () => {
  [null, undefined, "", " ", "-", "_"].forEach(testString => {
    expect(() => {
      labelize(testString);
    }).toThrow();
  });
});
