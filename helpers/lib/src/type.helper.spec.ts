/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {isset, Pot} from './type.helper';

describe('isset function tests', () => {
  it('Should return true', () => {
    let testValue: Pot<String> = "Hello World!";
    expect(isset<String>(testValue)).toBeTruthy();
  });

  it('Should return false', () => {
    let nullValue: Pot<String> = null;
    expect(isset<String>(nullValue)).toBeFalsy();

    let undefValue: Pot<String> = undefined;
    expect(isset<String>(undefValue)).toBeFalsy();

    let zeroValue: Pot<String> = "0";
    expect(isset<String>(zeroValue)).toBeTruthy();

    let emptyValue: Pot<String> = "";
    expect(isset<String>(emptyValue)).toBeTruthy();
  });
});
