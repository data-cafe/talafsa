/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

/**
 * Potentially a type: this type or undefined or null.
 */
export type Pot<T> = T | undefined | null;

/**
 * Check if a value is really set (not undefined nor null).
 * @param value a potentially value
 * @return boolean Check is a value is not undefined nor null.
 */
export function isset<T>(value: Pot<T>): boolean {
	return value !== undefined && value !== null;
}

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

/**
 * Mark a property optional of any type.
 */
export type WithOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
