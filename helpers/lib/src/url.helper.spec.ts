/*
 * data·café
 * Copyright (c) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import {addLeadingSlash, extractPureURI, removeEndingSlash} from './url.helper';
describe("URL helpers tests", () => {
  it('should remove ending slash', () => {
    expect(removeEndingSlash("blabla/")).toEqual("blabla");
    expect(removeEndingSlash("")).toEqual("");
    expect(removeEndingSlash("/")).toEqual("");
    expect(removeEndingSlash("text-without-slash")).toEqual("text-without-slash");
    expect(removeEndingSlash("/////////")).toEqual("////////");
  });

  it('should throw error', () => {
    [null, undefined].forEach(value => {
      expect(() => {
        removeEndingSlash(value);
      })
        .toThrow();
    });
  });

  it('should add leading slash', () => {
    expect(addLeadingSlash("blabla")).toEqual("/blabla");
    expect(addLeadingSlash("")).toEqual("/");
    expect(addLeadingSlash("text-without-slash")).toEqual("/text-without-slash");
    expect(addLeadingSlash("/////////")).toEqual("/////////");
    expect(addLeadingSlash("/")).toEqual("/");
  });

  it('should throw error', () => {
    [null, undefined].forEach(value => {
      expect(() => {
        addLeadingSlash(value);
      })
        .toThrow();
    });
  });

  it('should extract URI', () => {
    expect(extractPureURI("www.blabla.com/api/?=olala")).toEqual("www.blabla.com/api/");
    expect(extractPureURI("www.blabla.com/api/#userInfos")).toEqual("www.blabla.com/api/");
    expect(extractPureURI("")).toEqual("");
    expect(extractPureURI("/")).toEqual("/");
    expect(extractPureURI("text-without-slash")).toEqual("text-without-slash");
    expect(extractPureURI("/////////")).toEqual("/////////");
  });

  it('should throw error', () => {
    [null, undefined].forEach(value => {
      expect(() => {
        extractPureURI(value);
      })
        .toThrow();
    });
  });
});
