/*
 * data·café
 * Copyright (c) 2021-2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

/**
	* Simple helper method that remove an ending slash `/` if present
	*
	* @param url a valid URL
	*/
export function removeEndingSlash(url: string): string {
	return url[url.length - 1] === '/' ? url.slice(0, -1) : url;
}

/**
	* Simple helper method that add a leading slash `/` if not yet present
	*
	* @param url a valid URL
	*/
export function addLeadingSlash(url: string): string {
	return url[0] === '/' ? url : '/' + url;
}


/**
	* Extract only the URI from a path with optional query and fragments.
	*
	* For example, all these parameters will returns `/path`:
	*  - `/path`
	*  - `/path?query=thing`
	*  - `/path#framgent`
	*  - `/path?query=thing#framgent`
	*
	* @param path a complete path without server part.
	*/
export function extractPureURI(path: string): string {
	return path.split(/[?#]/)[0];
}
